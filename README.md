# Coding task
## To build and execute application:
1. Clone repository: ```git clone https://ivan_sukhorutchenko@bitbucket.org/ivan_sukhorutchenko/bg-coding-task.git```
1. Change directory: ```cd bg-coding-task```
1. Build project: ```mvn clean package```
1. Execute jar file: ```java -DinputFilePath=./input2.txt -jar ./target/bg-coding-task-jar-with-dependencies.jar```

## Note:
Input file path sets as JVM parameter to make the application more flexible. With such solution it is possible to get the parameter from any class of the application, there is no need to pass the parameter from main method to a certain class. Used OOTB feature to work with named parameters and it is possible to change name and/on number of parameters for other implementations of `com.bg.source.Source` interface.