package com.bg;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class ApplicationTest {

    @Test
    public void mainTest() {
        System.setProperty("inputFilePath", "./src/test/resources/input2.txt");

        PrintStream original = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos);
        System.setOut(out);

        String cars = "CARS:" + System.lineSeparator() +
                      "§  vw (7336a2c49b0045fa1340bf899f785e70)" + System.lineSeparator() +
                      "§  opel (f65b7d39472c52142ea2f4ea5e115d59)" + System.lineSeparator() +
                      "§  bmw (71913f59e458e026d6609cdb5a7cc53d)" + System.lineSeparator() +
                      "§  audi (4d9fa555e7c23996e99f1fb0e286aea8)";
        String animals = "ANIMALS:" + System.lineSeparator() +
                         "cow" + System.lineSeparator() +
                         "horse" + System.lineSeparator() +
                         "moose" + System.lineSeparator() +
                         "sheep";
        String numbers = "NUMBERS:" + System.lineSeparator() +
                         "six: 2" + System.lineSeparator() +
                         "one: 2" + System.lineSeparator() +
                         "seven: 1" + System.lineSeparator() +
                         "three: 2" + System.lineSeparator() +
                         "two: 1";

        Application.main(new String[]{});

        String consoleOutput = baos.toString();

        System.setOut(original);

        assertTrue(consoleOutput.contains(cars));
        assertTrue(consoleOutput.contains(animals));
        assertTrue(consoleOutput.contains(numbers));
        assertEquals(cars + System.lineSeparator() +
                     animals + System.lineSeparator() +
                     numbers + System.lineSeparator(),
                     consoleOutput);
    }

}
