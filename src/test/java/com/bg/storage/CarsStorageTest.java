package com.bg.storage;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bg.formatter.Formatter;

@RunWith(MockitoJUnitRunner.class)
public class CarsStorageTest {

    @Mock
    private Formatter formatter;

    @InjectMocks
    private CarsStorage target = new CarsStorage();

    @Test
    public void carsStorageTest() {
        Mockito.when(formatter.format(any(Map.Entry.class))).thenReturn("first car").thenReturn("second car");
        target.addToStore(target.getCategory());
        target.addToStore("test car");

        String expectedString = "first car;second car";

        String actualString = target.getPrintableStream().collect(Collectors.joining(";"));

        Mockito.verify(formatter, times(2)).format(any(Map.Entry.class));
        assertEquals(expectedString, actualString);
    }

}
