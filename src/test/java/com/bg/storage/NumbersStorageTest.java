package com.bg.storage;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;

import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bg.formatter.Formatter;

@RunWith(MockitoJUnitRunner.class)
public class NumbersStorageTest {

    @Mock
    private Formatter formatter;

    @InjectMocks
    private NumbersStorage target = new NumbersStorage();

    @Test
    public void numbersStorageTest() {
        Mockito.when(formatter.format(any(Map.Entry.class))).thenReturn("first number").thenReturn("second number");
        target.addToStore(target.getCategory());
        target.addToStore("test number");

        String expectedString = "first number;second number";

        String actualString = target.getPrintableStream().collect(Collectors.joining(";"));

        Mockito.verify(formatter, times(2)).format(any(Map.Entry.class));
        assertEquals(expectedString, actualString);
    }

}
