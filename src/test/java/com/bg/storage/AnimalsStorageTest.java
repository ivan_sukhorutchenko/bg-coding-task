package com.bg.storage;

import static org.junit.Assert.assertEquals;

import java.util.stream.Collectors;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.bg.formatter.Formatter;

@RunWith(MockitoJUnitRunner.class)
public class AnimalsStorageTest {

    @Mock
    private Formatter formatter;

    @InjectMocks
    private AnimalsStorage target = new AnimalsStorage();

    @Test
    public void animalsStorageTest() {
        Mockito.when(formatter.format("ANIMALS")).thenReturn("ANIMALS");
        Mockito.when(formatter.format("test animal")).thenReturn("test animal");
        target.addToStore(target.getCategory());
        target.addToStore("test animal");

        String expectedString = "ANIMALS;test animal";

        String actualString = target.getPrintableStream().collect(Collectors.joining(";"));

        Mockito.verify(formatter).format("ANIMALS");
        Mockito.verify(formatter).format("test animal");
        assertEquals(expectedString, actualString);
    }

}
