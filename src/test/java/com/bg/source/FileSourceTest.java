package com.bg.source;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.stream.Stream;

import org.junit.Test;

public class FileSourceTest {

    private FileSource target = new FileSource();

    @Test
    public void shouldOpenAndReadFile() {
        System.setProperty("inputFilePath", "./src/test/resources/two_lines.txt");
        assertEquals(2, target.getSourceStream().count());
    }

    @Test
    public void fileShouldNotBeFound() {
        System.setProperty("inputFilePath", "./fake/path/fake.txt");

        PrintStream original = System.err;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream err = new PrintStream(baos);
        System.setErr(err);

        String expected = "File not found!" + System.lineSeparator();

        Stream<String> sourceStream = target.getSourceStream();

        System.setErr(original);

        assertEquals(0, sourceStream.count());
        assertEquals(expected, baos.toString());
    }

}
