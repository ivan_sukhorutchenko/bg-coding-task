package com.bg.output;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Test;

public class SystemConsoleOutputTest {

    private SystemConsoleOutput target = new SystemConsoleOutput();

    @Test
    public void write() {
        PrintStream original = System.out;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(baos);
        System.setOut(out);

        String expectedString = "expectedString";
        target.write(expectedString);
        String actualString = baos.toString();

        assertEquals(expectedString + System.lineSeparator(), actualString);

        System.setOut(original);
    }

}
