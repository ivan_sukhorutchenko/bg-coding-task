package com.bg.formatter;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class AnimalsFormatterTest {

    private AnimalsFormatter target = new AnimalsFormatter();

    @Test
    public void format() {
        String expectedString = "expectedString";
        assertEquals(expectedString, target.format(expectedString));
    }

}
