package com.bg.formatter;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class CarsFormatterTest {

    private CarsFormatter target = new CarsFormatter();

    @Test
    public void format() {
        Map<String, String> testMap = new HashMap<>();
        testMap.put("test car", "377b7223aa433915c0e98fd1b4df029f");

        String expectedString = "§  test car (377b7223aa433915c0e98fd1b4df029f)";

        assertEquals(expectedString, target.format(testMap.entrySet().stream().findFirst().get()));
    }
}
