package com.bg.formatter;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class NumbersFormatterTest {

    private NumbersFormatter target = new NumbersFormatter();

    @Test
    public void format() {
        Map<String, Integer> testMap = new HashMap<>();
        testMap.put("test animal", 4);

        String expectedString = "test animal: 4";

        assertEquals(expectedString, target.format(testMap.entrySet().stream().findFirst().get()));
    }

}
