package com.bg.storage;

import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

public interface Storage {

    @NotNull
    String getCategory();

    void addToStore(@NotNull String item);

    @NotNull
    Stream<String> getPrintableStream();

}
