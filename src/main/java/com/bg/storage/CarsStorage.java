package com.bg.storage;

import java.util.Collections;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Stream;

import org.apache.commons.codec.digest.DigestUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bg.formatter.Formatter;

@Component
@Qualifier("CarsStorage")
public class CarsStorage implements Storage {

    @Autowired
    @Qualifier("CarsFormatter")
    private Formatter formatter;

    private Map<String, String> storage = new TreeMap<>(Collections.reverseOrder());

    @NotNull
    @Override
    public String getCategory() {
        return "CARS";
    }

    @Override
    public void addToStore(@NotNull String item) {
        storage.computeIfAbsent(item.toLowerCase(), key -> DigestUtils.md5Hex(key.getBytes()));
    }

    @NotNull
    @Override
    public Stream<String> getPrintableStream() {
        return storage.entrySet()
                .stream()
                .map(formatter::format);
    }

}
