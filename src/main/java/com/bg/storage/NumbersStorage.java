package com.bg.storage;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bg.formatter.Formatter;

@Component
@Qualifier("NumbersStorage")
public class NumbersStorage implements Storage {

    @Autowired
    @Qualifier("NumbersFormatter")
    private Formatter formatter;

    private static final int ONE = 1;
    private Map<String, Integer> storage = new HashMap<>();

    @NotNull
    @Override
    public String getCategory() {
        return "NUMBERS";
    }

    @Override
    public void addToStore(@NotNull String item) {
        storage.computeIfPresent(item, (k, v) -> ++v);
        storage.putIfAbsent(item, ONE);
    }

    @NotNull
    @Override
    public Stream<String> getPrintableStream() {
        return storage.entrySet()
                .stream()
                .map(formatter::format);
    }

}
