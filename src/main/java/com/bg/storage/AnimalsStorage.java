package com.bg.storage;

import java.util.Set;
import java.util.TreeSet;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.bg.formatter.Formatter;

@Component
@Qualifier("AnimalsStorage")
public class AnimalsStorage implements Storage {

    @Autowired
    @Qualifier("AnimalsFormatter")
    private Formatter formatter;

    private Set<String> storage = new TreeSet<>();

    @NotNull
    @Override
    public String getCategory() {
        return "ANIMALS";
    }

    @Override
    public void addToStore(@NotNull String item) {
        storage.add(item);
    }

    @NotNull
    @Override
    public Stream<String> getPrintableStream() {
        return storage.stream()
                .map(formatter::format);
    }

}
