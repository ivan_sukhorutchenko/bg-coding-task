package com.bg.formatter;

import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("NumbersFormatter")
public class NumbersFormatter implements Formatter {

    @NotNull
    @Override
    public String format(@NotNull Object item) {
        Map.Entry<String, Integer> entry = (Map.Entry<String, Integer>) item;
        return new StringBuilder(entry.getKey())
                .append(": ")
                .append(entry.getValue())
                .toString();
    }

}
