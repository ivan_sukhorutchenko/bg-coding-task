package com.bg.formatter;

import org.jetbrains.annotations.NotNull;

public interface Formatter {

    @NotNull
    String format(@NotNull Object item);

}
