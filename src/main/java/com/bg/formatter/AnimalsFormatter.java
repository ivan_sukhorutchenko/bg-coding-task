package com.bg.formatter;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("AnimalsFormatter")
public class AnimalsFormatter implements Formatter {

    @NotNull
    @Override
    public String format(@NotNull Object item) {
        return item.toString();
    }

}
