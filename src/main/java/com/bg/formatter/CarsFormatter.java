package com.bg.formatter;

import java.util.Map;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("CarsFormatter")
public class CarsFormatter implements Formatter {

    @NotNull
    @Override
    public String format(@NotNull Object item) {
        Map.Entry<String, String> entry = (Map.Entry<String, String>) item;
        return new StringBuilder("§  ")
                .append(entry.getKey())
                .append(" (")
                .append(entry.getValue())
                .append(")")
                .toString();
    }

}
