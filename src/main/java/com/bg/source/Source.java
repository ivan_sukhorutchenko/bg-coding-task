package com.bg.source;

import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;

public interface Source {

    @NotNull
    Stream<String> getSourceStream();

}
