package com.bg.source;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.stream.Stream;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("FileSource")
public class FileSource implements Source {

    @NotNull
    @Override
    public Stream<String> getSourceStream() {
        return Optional.ofNullable(System.getProperty("inputFilePath"))
                .map(this::streamFromFile)
                .orElse(Stream.empty());
    }

    @NotNull
    private Stream<String> streamFromFile(@NotNull String fileName) {
        try {
            return Files.lines(Paths.get(fileName));
        } catch (IOException e) {
            System.err.println("File not found!");
            return Stream.empty();
        }
    }

}
