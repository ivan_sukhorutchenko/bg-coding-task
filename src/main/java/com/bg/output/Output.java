package com.bg.output;

import org.jetbrains.annotations.NotNull;

public interface Output {

    void write(@NotNull String item);

}
