package com.bg.output;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
@Qualifier("SystemConsoleOutput")
public class SystemConsoleOutput implements Output {

    @Override
    public void write(@NotNull String item) {
        System.out.println(item);
    }

}
