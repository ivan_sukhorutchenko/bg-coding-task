package com.bg;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.bg.storage.Storage;

@Configuration
public class ApplicationConfiguration {

    @Autowired
    private List<Storage> storageList;

    @Bean
    public Map<String, Storage> storageMap() {
        return storageList.stream()
                .collect(Collectors.toMap(Storage::getCategory, Function.identity()));
    }

}
