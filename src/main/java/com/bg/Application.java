package com.bg;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.bg.processor.Processor;

public class Application {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
        context.scan("com.bg");
        context.register(ApplicationConfiguration.class);
        context.refresh();

        Processor processor = context.getBean(Processor.class);
        processor.process();
    }

}
