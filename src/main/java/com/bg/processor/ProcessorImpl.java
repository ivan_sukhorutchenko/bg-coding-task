package com.bg.processor;

import java.util.Map;
import java.util.Optional;

import javax.annotation.Resource;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bg.output.Output;
import com.bg.source.Source;
import com.bg.storage.Storage;

@Component
public class ProcessorImpl implements Processor {

    @Autowired
    private Source source;

    @Resource
    private Map<String, Storage> storageMap;

    @Autowired
    private Output output;

    private Optional<Storage> currentStorage = Optional.empty();

    @Override
    public void process() {
        source.getSourceStream().forEach(this::store);
        storageMap.entrySet().forEach(this::printResult);
    }

    /**
     * Starting Java 9 we can use <link>https://docs.oracle.com/javase/9/docs/api/java/util/Optional.html#ifPresentOrElse-java.util.function.Consumer-java.lang.Runnable-</link>
     */
    private void store(@NotNull String line) {
        Optional<Storage> optionalStorage = Optional.ofNullable(storageMap.get(line.toUpperCase()));
        currentStorage = optionalStorage.map(storage -> optionalStorage)
                .orElseGet(() -> {
                    currentStorage.ifPresent(storage -> storage.addToStore(line));
                    return currentStorage;
                });
    }

    private void printResult(@NotNull Map.Entry<String, Storage> storageEntry) {
        output.write(storageEntry.getKey() + ":");
        storageEntry.getValue()
                .getPrintableStream()
                .forEach(output::write);
    }

}
